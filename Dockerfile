FROM debian AS build
RUN apt-get update && apt-get install -y hugo

COPY . /site
WORKDIR /site

RUN hugo

FROM nginx
RUN rm /usr/share/nginx/html/*
COPY --from=build /site/public /usr/share/nginx/html
